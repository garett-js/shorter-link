![short URL](https://pp.userapi.com/c845221/v845221879/d43f6/ajJSLdqziHs.jpg)


```sh
$ git clone https://garett-js@bitbucket.org/garett-js/shorter-link.git
```
- Создать БД short_url

```sh
mysql> source db.sql;
```

- Конфиг в директории /config/config.php.dist (переименовать в /config/config.php) и изменить значения на свои

```php
'database' => [
    'name'       => 'short_url',
    'username'   => 'root',
    'password'   => '',
    'connection' => 'mysql:host=127.0.0.1',
    'options'    => [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION   
    ]
],
```

- Перейти по адресу http://ваш_домен/index.php

- Ввести в поле полный URL и нажать на кнопку Сократить URL

- Ниже будет короткая ссылка типа http://ваш_домен/aBcDe
