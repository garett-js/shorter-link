<?php


class QueryBuilder
{
    protected $pdo;

    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function insert($table, $parameters)
    {
        $sql = "INSERT INTO {$table}(short_url, url, created, updated)
                       VALUES (:short_url, :url, NOW(), NOW())";
        try {
            $statement = $this->pdo->prepare($sql);
            $statement->execute($parameters);
        } catch (Exception $e) {
            // check for duplicate
            if ($e->getCode() == 23000) {
              $this->getShortUrl($table, $parameters);
            } else {
              die($e->getMessage());
            }
        }
    }

    public function getUrl($table, $shortUrl)
    {
        $statement = $this->pdo->prepare("SELECT url FROM {$table} WHERE short_url = :short_url");
        $statement->execute(['short_url' => $shortUrl]);

        return $statement->fetch(PDO::FETCH_LAZY);
    }

    public function getShortUrl($table, $parameters)
    {
      $statement = $this->pdo->prepare("SELECT short_url FROM {$table} WHERE url = :url");
      $statement->execute(['url' => $parameters['url']]);

      return $statement->fetch(PDO::FETCH_LAZY)[0];
    }
}
