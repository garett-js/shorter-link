<?php


class ShortURL
{
    public static function make($url, $length)
    {
        App::get('database')->insert('links', [
            'short_url' => substr(str_shuffle(implode(range('a', 'z')) . implode(range('A', 'Z')) ), 0, $length),
            'url' => $url
        ]);
    }

    public static function getOriginURL($shortValue)
    {
        return App::get('database')->getUrl('links', $shortValue);
    }

    public static function getShortURL($url)
    {
        return App::get('database')->getShortUrl('links', [
            'url' => $url
        ]);
    }
}
