<?php


require "App.php";
require "database/Connection.php";
require "database/QueryBuilder.php";
require "ShortURL.php";
require "Request.php";

App::bind('config', require 'config/config.php');
App::bind('database', new QueryBuilder(
    Connection::make(App::get('config')['database'])
));
