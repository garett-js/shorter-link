<?php

require "core/bootstrap.php";

if (!empty($_POST["url"]) && filter_var($_POST["url"], FILTER_VALIDATE_URL)) {

    ShortURL::make($_POST["url"], 15);

    echo "http://" . $_SERVER['HTTP_HOST'] . "/" . ShortURL::getShortURL($_POST["url"]);
} else {
    echo "Неверный формат URL";
}
