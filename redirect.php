<?php
require "core/bootstrap.php";

if ($url = ShortURL::getOriginURL(Request::uri())) {
    header("Location: {$url['url']}");
    exit();
}
