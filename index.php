<?php require "redirect.php"; ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Сокращатель URL</title>
</head>
<body>

  <div class="mb-50"></div>

  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-7 col-md-7 col-sm-9">
        <div class="card card-animation">
          <h5 class="card-header text-center">Сокращатель URL</h5>
          <div class="card-body">

            <form id="form_url">
              <div class="form-group">
                <label for="inputPassword2" class="sr-only">URL</label>
                <input class="form-control" type="url" name="url" id="fullURL" placeholder="Вставьте URL">
              </div>
              <div class="text-center">
                <input type="button" id="btn" class="btn btn-animated btn-warning mb-2" value="Сократить"/>
              </div>
            </form>

          </div>
          <div class="card-footer text-center">php + mysql + bootstrap # основы</div>
        </div>
      </div>
    </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="/assets/js/ajax.js"></script>
</body>

</html>
